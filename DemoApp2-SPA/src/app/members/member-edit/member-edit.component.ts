import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { User } from 'src/app/_models/user';
import { ActivatedRoute } from '@angular/router';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/_services/auth.service';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-member-edit',
  templateUrl: './member-edit.component.html',
  styleUrls: ['./member-edit.component.css']
})
export class MemberEditComponent implements OnInit {
  // we need this in order to access the form
  @ViewChild('editForm', {static:true}) editForm: NgForm;
  // properties:
  user: User;
  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    // this handles event when user tries to close browser tab
    if (this.editForm.dirty) {
      $event.returnValue = true;
    }
  }

  // bring in activated route so we have access to the data
  constructor(private route: ActivatedRoute, private alertify: AlertifyService,
              private userService: UserService, private authService: AuthService) { }

  ngOnInit() {
    // asign this the data from our route
    this.route.data.subscribe(data => {
      this.user = data['user'];
    });
  }

  updateUser() {
    this.userService.updateUser(this.authService.decodedToken.nameid, this.user).subscribe(next => {
      this.alertify.success('Profile updated successfully');
      // using ViewChild directive above allows us to access editForm's methods
      // tslint:disable-next-line: max-line-length
      this.editForm.reset(this.user); // this will reset dirty form back to clean, but passing the this.user param will load the values to its previous save values, instead of wiping out all inputs
    }, error => {
      this.alertify.error(error);
    });
  }

}
