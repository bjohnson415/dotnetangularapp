namespace DemoApp.API.Dtos
{
    public class UserForUpdateDto
    {
        // add the properties that we are allowing the user to edit. Then we will map these fields to our actual User object in AutoMapperProfiles.cs
        public string Introduction { get; set; }  

        public string LookingFor { get; set; }  

        public string Interests { get; set; }  

        public string City { get; set; }  

        public string Country { get; set; }  
    }
}